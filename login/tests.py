from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import index

from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

# Create your tests here.
class UnitTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_is_exist(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_home_using_template_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_login_using_template_index(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')

class FuncionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FuncionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FuncionalTest, self).tearDown()
    
    def test_functional(self):
        self.browser.get('http://localhost:8000/')

        #Test Judul
        self.assertIn('HOME', self.browser.title)

        #Test Tombol Login
        tulisan = self.browser.find_element_by_id('home').text
        self.assertIn('login', tulisan)

        #Test Login Page
        login_btn = self.browser.find_element_by_tag_name('a')
        login_btn.click()
        time.sleep(5)
        result = self.browser.find_element_by_class_name('container-box').text
        self.assertIn('Login', result)
        self.assertIn('Login', self.browser.title)
        uname = self.browser.find_element_by_id('id_username')
        pw = self.browser.find_element_by_id('id_password')
        uname.send_keys('user')
        pw.send_keys('inicontohaja')
        login_btn = self.browser.find_element_by_tag_name('button')
        login_btn.click()
        time.sleep(5)
        self.assertIn('HOME', self.browser.title)
        result = self.browser.find_element_by_id('home').text
        self.assertIn('logout', result)
        self.browser.quit()


if __name__ == '__main__':
    unittest.main(warnings='ignore')